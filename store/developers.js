import client from '../plugins/contentful'

export const state = () => ({
  developers: []
})

export const mutations = {
  setHeadDeveloper(state, payload) {
    state.headDeveloper = payload
  },
  setDevelopers(state, payload) {
    state.developers = payload
  }
}

export const actions = {
  async getDevelopers({ commit }) {
    const response = await client.getEntries({
      content_type: 'developer'
    })
    if (response.items.length > 0) {
      commit('setDevelopers', response.items)
    }
  },
  async getHeadDeveloper({ commit }) {
    const response = await client.getEntries({
      content_type: 'developer',
      'fields.lineManager': null
    })
    if (response.items.length > 0) {
      commit('setHeadDeveloper', response.items[0])
    }
  }
}
